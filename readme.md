# Подключение к серверу
ssh <your_server_name>@<server_public_ip>

# Обновление менеджера пакетов
sudo apt-get update

# Установка docker, docker-compose
sudo apt install docker docker-compose

# Клонирование проекта из репозитория
git clone git@gitlab.com:skypro7074113/cw.git

# Копирование файла с переменными окружения
cp .env.example .env

# Настройка переменных окружения
vim .env

# Разрешаем запуск Docker из-под пользователя
sudo usermod -aG docker $USER

# Даём доступ пользователю к docker socket
sudo chmod +666 /var/run/docker.sock

# Запускаем docker контейнеры в фоновом режиме
docker-compose up -d